// Copyright (c) Wikimedia Foundation and contributors.
// All Rights Reserved.
//
// This file is part of Bridgebot
//
// Bridgebot is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Bridgebot is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with Bridgebot. If not, see <http://www.gnu.org/licenses/>.
//
// Adapted from https://github.com/42wim/matterbridge/blob/56e7bd01ca09ad52b0c4f48f146a20a4f1b78696/matterbridge.go
/*
Bridgebot is a lightly customized deployment of the matterbridge chat bridge.

See https://github.com/42wim/matterbridge for more on matterbridge.
*/
package main

import (
	"flag"
	"fmt"
	"os"
	"runtime"
	"strings"

	"github.com/42wim/matterbridge/bridge/config"
	"github.com/42wim/matterbridge/gateway"
	"github.com/42wim/matterbridge/gateway/bridgemap"
	"github.com/joho/godotenv"
	prefixed "github.com/matterbridge/logrus-prefixed-formatter"
	"github.com/sirupsen/logrus"
)

var flagConfig = flag.String("conf", "etc/bridgebot.toml", "config file")

func main() {
	flag.Parse()

	rootLogger := &logrus.Logger{
		Out: os.Stderr,
		Formatter: &prefixed.TextFormatter{
			PrefixPadding: 13,
			DisableColors: true,
			FullTimestamp: false,
			CallerFormatter: func(function, file string) string {
				return fmt.Sprintf(" [%s:%s]", function, file)
			},
			CallerPrettyfier: func(f *runtime.Frame) (string, string) {
				filename := f.File
				sp := strings.SplitAfter(f.File, "/toolforge-repos/")
				if len(sp) > 1 {
					filename = sp[1]
				}
				sp = strings.SplitAfter(f.File, "/matterbridge/")
				if len(sp) > 1 {
					filename = sp[1]
				}
				s := strings.Split(f.Function, ".")
				funcName := s[len(s)-1]
				return funcName, fmt.Sprintf("%s:%d", filename, f.Line)
			},
		},
		Level:        logrus.DebugLevel,
		ReportCaller: true,
	}
	logger := rootLogger.WithFields(logrus.Fields{"prefix": "main"})

	// Attempt to read envvars from a .env file
	err := godotenv.Load()
	if err == nil {
		logger.Info("Loaded envvars from .env")
	}

	cfg := config.NewConfig(rootLogger, *flagConfig)
	cfg.BridgeValues().General.Debug = true

	r, err := gateway.NewRouter(rootLogger, cfg, bridgemap.FullMap)
	if err != nil {
		logger.Fatalf("Starting gateway failed: %s", err)
	}
	if err = r.Start(); err != nil {
		logger.Fatalf("Starting gateway failed: %s", err)
	}
	logger.Printf("Gateway(s) started successfully. Now relaying messages")
	select {}
}
