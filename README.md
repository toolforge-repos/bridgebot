Bridgebot
=========

Bridgebot is a lightly customized deployment of the FLOSS [matterbridge][]
software. It can be used to relay messages between multiple different chat
systems.

Build
-------

Build `bridgebot` locally:
```
$ go mod tidy
$ go fmt
$ CGO_ENABLED=0 go build -mod=mod -o bridgebot
```

Build a container for Toolforge:
```
$ ssh login.toolforge.org
$ become bridgebot
$ toolforge build start https://gitlab.wikimedia.org/toolforge-repos/bridgebot
```

License
-------
[GPL-3.0-or-later](https://www.gnu.org/licenses/gpl-3.0.html)

[matterbridge]: https://github.com/42wim/matterbridge/
